package controllers;

import model.HandCharacter;

public class PasswordChecker {
	private HandCharacter[] currentInput;
	private HandCharacter[] actualPassword;

	public double ACCEPTABLE_FINGER_DISTANCE = 3000;
	public double ACCEPTABLE_ROLL = 20;
	public double ACCEPTABLE_PITCH = 20;

	public PasswordChecker(HandCharacter[] Input, HandCharacter[] Password) {
		currentInput = Input;
		actualPassword = Password;
	}

	public boolean check() {
		int passwordSize = currentInput.length;
		if (passwordSize != actualPassword.length) {
			return false;
		}

		// Check all of the different hand characters
		for (int i = 0; i < passwordSize; i++) {
			if (!checkHandCharacter(currentInput[i], actualPassword[i])) {
				return false;
			}
		}

		return true;
	}

	public boolean checkHandCharacter(HandCharacter current, HandCharacter pass) {
		float ctDistance = current.getThumbPosition().distanceTo(current.getPalmPosition());
		float ptDistance = pass.getThumbPosition().distanceTo(pass.getPalmPosition());
		boolean thumb = checkFinger(ctDistance, ptDistance);
		
		float ciDistance = current.getIFingerPosition().distanceTo(current.getPalmPosition());
		float piDistance = pass.getIFingerPosition().distanceTo(pass.getPalmPosition());
		boolean index = checkFinger(ciDistance, piDistance);
		
		float cmDistance = current.getMFingerPosition().distanceTo(current.getPalmPosition());
		float pmDistance = pass.getMFingerPosition().distanceTo(pass.getPalmPosition());
		boolean middle = checkFinger(cmDistance, pmDistance);
		
		float crDistance = current.getRFingerPosition().distanceTo(current.getPalmPosition());
		float prDistance = pass.getRFingerPosition().distanceTo(pass.getPalmPosition());
		boolean ring = checkFinger(crDistance, prDistance);
		
		float cpDistance = current.getPFingerPosition().distanceTo(current.getPalmPosition());
		float ppDistance = pass.getPFingerPosition().distanceTo(pass.getPalmPosition());
		boolean pinky = checkFinger(cpDistance, ppDistance);
		
		float cpPitch = current.getPitch();
		float pcPitch = pass.getPitch();
		boolean pitch = Math.abs(cpPitch-pcPitch) <= ACCEPTABLE_PITCH ;
		
		float cpRoll = current.getRoll();
		float pcRoll = pass.getRoll();
		boolean roll = Math.abs(cpRoll-pcRoll) <= ACCEPTABLE_ROLL ;

		if (thumb && index && middle && ring && pinky && pitch && roll) {
			return true;
		} else {
			return false;
		}

	}

	public boolean checkFinger(float currentPosition, float passPosition) {
		float distance = currentPosition - passPosition;
		float absDistance = Math.abs(distance);

		if (absDistance < 5) {
			return true;
		} else {
			return false;
		}
	}

	

	public void setAcceptableFingerDistance(double acceptableFingerDistance) {
		this.ACCEPTABLE_FINGER_DISTANCE = acceptableFingerDistance;
	}


}
