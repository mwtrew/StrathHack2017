package model; /**
 * Created on 11/02/17.
 */
/******************************************************************************\
 * Copyright (C) 2012-2013 Leap Motion, Inc. All rights reserved.               *
 * Leap Motion proprietary and confidential. Not for distribution.              *
 * Use subject to the terms of the Leap Motion SDK Agreement available at       *
 * https://developer.leapmotion.com/sdk_agreement, or another agreement         *
 * between Leap Motion and you, your company or other organization.             *
 \******************************************************************************/

import java.lang.Math;
import com.leapmotion.leap.*;
import com.leapmotion.leap.Gesture.State;

public class DataListener extends Listener {
	
	
	Vector palmPos;
	Vector tVector;
	Vector fVector;
	Vector mVector;
	Vector rVector;
	Vector pVector;
	float pitch;
	float roll;
	
	public static Password pw;
	
	
	
    public void onInit(Controller controller) {
        System.out.println("Initialized");
    }
    
    

    public void onConnect(Controller controller) {
        System.out.println("Connected");
    }

    public void onDisconnect(Controller controller) {
        //Note: not dispatched when running in a debugger.
        System.out.println("Disconnected");
    }

    public void onExit(Controller controller) {
        System.out.println("Exited");
    }

    public void onFrame(Controller controller) {
        // Get the most recent frame and report some basic information
        Frame frame = controller.frame();

//        System.out.println("Frame id: " + frame.id()
//                + ", timestamp: " + frame.timestamp()
//                + ", hands: " + frame.hands().count()
 //               + ", fingers: " + frame.fingers().count()
 //               + ", tools: " + frame.tools().count()
  //              + ", gestures " + frame.gestures().count());

        //Get hands
        for(Hand hand : frame.hands()) {
            String handType = hand.isLeft() ? "Left hand" : "Right hand";
      //      System.out.println("  " + handType + ", id: " + hand.id()
     //               + ", palm position: " + hand.palmPosition());

//            Get the hand's normal vector and direction
              Vector normal = hand.palmNormal();
              Vector direction = hand.direction();
              palmPos = hand.palmPosition();
//
//            // Calculate the hand's pitch, roll, and yaw angles
       //       System.out.println("  pitch: " + Math.toDegrees(direction.pitch()) + " degrees, "
        //              + "roll: " + Math.toDegrees(normal.roll()) + " degrees");
              pitch = direction.pitch();
              roll = normal.roll();
              

          //   Get fingers
              for (Finger finger : hand.fingers()) {
                 // System.out.println(finger.type());
                  switch(finger.type()){
                  	case TYPE_THUMB:
                  		tVector = finger.tipPosition();
                  	case TYPE_INDEX:
                  		fVector = finger.tipPosition();
                  	case TYPE_MIDDLE:
                		mVector = finger.tipPosition();
                 	case TYPE_RING:
                		rVector = finger.tipPosition();
                	case TYPE_PINKY:
               		pVector = finger.tipPosition();
              	}
                }
        }
  		//	  }    
              }

    
    public HandCharacter capture(){

    	 return new HandCharacter(palmPos, pitch, roll, tVector, fVector, mVector, rVector, pVector);
 	 
    }
    
    public Password getPassword() {
    	return DataListener.pw;
    }
    
    public void setPassword(Password pw) {
    	DataListener.pw = pw;
    }
}

