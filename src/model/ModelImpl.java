package model;

import com.leapmotion.leap.Controller;
import com.leapmotion.leap.GestureList;

import java.io.IOException;
import java.util.Observable;

/**
 * Created on 12/02/17.
 */
public class ModelImpl extends Observable implements Model{

    private enum Mode {MONITOR, RECORD};
    private Mode mode;
    //private Password inputPass;
    private Password storedPass;
    private ComplexListener listener;


    public ModelImpl(){


    }

    public void startInputStream(){
        ComplexListener listener = new ComplexListener();
        Controller controller = new Controller();
        GestureList gl = new GestureList();

        // Have the sample listener receive events from the controller
        controller.addListener(listener);

        // Keep this process running until Enter is pressed
        System.out.println("Press Enter to quit...");
        try {
            System.in.read();
            gl = listener.getGestureList();
            System.out.println(gl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Remove the sample listener when done
        controller.removeListener(listener);
        listener = new ComplexListener();
    }

    public void monitor(){
        this.mode = Mode.MONITOR;
        System.out.println("ModelImpl.monitor (monitor update attempt)");
        setChanged();
        notifyObservers("monitor");

        /*
        for (int i=0; i <= storedPass.size(); i++){

            //todo: wait until the input handchar equals storedPass.get(i)
            notifyObservers(i); //give the view the no. of matched gestures
        }
        */

    }

    public void record(){
        this.mode = Mode.RECORD;

        setChanged();
        System.out.println("ModelImpl.record (monitor update attempt)");


        notifyObservers("record");
    }









}
