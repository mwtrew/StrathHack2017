import controller.FrameListener;
import controller.MonitorPanelListener;
import controller.RecordPanelListener;
import model.ModelImpl;
import model.SampleListener;
import view.GUIFrame;
import view.MonitorPanel;
import view.RecordPanel;

import java.awt.*;

import com.leapmotion.leap.Controller;

/**
 * Created on 11/02/17.
 */
class Driver {
    public static void main(String[] args) {

        
        MonitorPanel monitorView = new MonitorPanel();
        RecordPanel recordView = new RecordPanel();
        GUIFrame view = new GUIFrame(monitorView, recordView);

        ModelImpl model = new ModelImpl(); //this should be Model
        model.addObserver(view);
        model.addObserver(monitorView);
        model.addObserver(recordView);


        //

        FrameListener controller = new FrameListener();
        controller.addModel(model);
        controller.addView(view);

        System.out.println("Driver.main (controller/listener added to frame)");
        view.addController(controller);


        //

        //

        MonitorPanelListener monitorController = new MonitorPanelListener();
        monitorController.addModel(model);
        monitorController.addView(monitorView);

        monitorView.addController(monitorController);

        //

        //

        RecordPanelListener recordController = new RecordPanelListener();
        recordController.addModel(model);
        recordController.addView(recordView);

        recordView.addController(recordController);

        //

    }
}
