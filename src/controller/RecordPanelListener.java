package controller;

import model.Model;
import model.ModelImpl;
import view.RecordPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created on 11/02/17.
 */
public class RecordPanelListener implements ActionListener {

    ModelImpl m;
    RecordPanel r;

    public RecordPanelListener(){

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
    }

    public void addModel(ModelImpl model){
        this.m = model;
    }

    public void addView(RecordPanel recordPanel){
        this.r = recordPanel;
    }
}
