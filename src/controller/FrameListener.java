package controller;

import model.Model;
import model.ModelImpl;
import view.GUIFrame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created on 11/02/17.
 */
public class FrameListener implements ActionListener{

    ModelImpl model; //this should be model
    GUIFrame view;

    public FrameListener(){

    }

    public void actionPerformed(ActionEvent e){

        switch (e.getActionCommand()){
            case "Set handshake":
                System.out.println("FrameListener.actionPerformed (record action command attempt)");
                record();
                break;

            case "Compare":
                System.out.println("FrameListener.actionPerformed (compare action command attempt)");
                monitor();
                break;

        }

    }

    private void monitor(){
        model.monitor();
    }
    private void record(){
        model.record();
    }

    public void addModel(ModelImpl m){ //this should be model
        this.model = m;
    }

    public void addView(GUIFrame f)

    {
        this.view = f;
    }

}
