# StrathHack 2017 Project: "Secret Handshake"

Using hand symbols as an authentication mechanism - i.e. records a series of
symbols as the password, then monitors the user's hand and notifies the user
if they repeat the recorded series correctly. Requires LeapMotion controller and
API.

![sh](https://challengepost-s3-challengepost.netdna-ssl.com/photos/production/software_photos/000/472/990/datas/gallery.jpg)

## Team Redacted; Reason - Not Cool

Need to add this to the VM arguments for run config: 

-Djava.library.path="/home/mloaf/Documents/Code/idea/StrathHack2017/src/lib"

https://api.leapmotion.com/documentation/java/index.html


Gestures are considered analogous to characters in a password  - i.e the series
of the gestures is important but the timing isn't. Record a keyframe
